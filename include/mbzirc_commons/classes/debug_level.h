#ifndef __DEBUG_LEVEL_H_INCLUDED__
#define __DEBUG_LEVEL_H_INCLUDED__

namespace mbzirc
{
namespace commons
{

  class DebugLevel
  {
  public:
    static bool ShowImages;
    static bool PrintInfo;

    static bool SetDebugLevel(int debug_level);
  };

} // end of commons namespace

} // end of mbzirc namespace

#endif // __DEBUG_LEVEL_H_INCLUDED__
