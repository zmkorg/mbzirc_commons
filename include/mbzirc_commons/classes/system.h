#ifndef __MBZIRC_MISC_H_INCLUDED__
#define __MBZIRC_MISC_H_INCLUDED__

#include <string>
#include <vector>

namespace mbzirc
{
namespace commons
{

  class System
  {
  public:
    static std::string GetHomeDir();
    static std::vector<std::string> ListSubDirectories(std::string path);
    static bool CheckFileExtension(std::string filename, std::string extension);
    static std::vector<std::string> ListFiles(std::string path, std::string extension = "", std::string exclude_prefix = "");
  };


} // end of commons namespace

} // end of mbzirc namespace

# endif // __MBZIRC_MISC_H_INCLUDED__
