#ifndef __FILTERS_H_INCLUDED__
#define __FILTERS_H_INCLUDED__ 

#include <vector>
#include <algorithm>

namespace mbzirc
{
namespace commons
{
  
  class MedianFilter 
  {
  private:
    std::vector<double> buff_;
    int nbuff_; 
    
  public:
    MedianFilter(int nbuff):nbuff_(nbuff) { }
    void reset();
    double filter(double value);
  };

} // end of commons namespace

} // end of mbzirc namespace

#endif // FILTERS_H_INCLUDED__