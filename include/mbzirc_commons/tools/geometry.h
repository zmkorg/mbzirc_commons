#ifndef __GEOMETRY_H_INCLUDED__
#define __GEOMETRY_H_INCLUDED__ 

#include <cv_bridge/cv_bridge.h>

namespace mbzirc
{
namespace commons
{

  class Geometry
  {
  public:
    
    /// Returns the intersect point of two lines
    static cv::Point2f LinesIntersection(cv::Point2f a1, cv::Point2f b1, cv::Point2f a2, cv::Point2f b2);

    /// Returns the Cartesian coefficients for a line described in polar coordinates
    /// The line will be a*x + by = a*x0 + by0, where (x0, y0) is the returned point
    static cv::Point2f LinePolarToCartesian(double rho, double theta, double &a, double &b);
    
    /// Returns the distance of a line from a point in cartesian coordinates
    /// The line is described with a*x + b*y = a*x0 + b*y0, where p0 = (x0, y0)
    static double LineDistFromPoint(double a, double b, cv::Point2f p0, cv::Point2f point);
    
  };

} // end of commons namespace

} // end of mbzirc namespace

#endif // __GEOMETRY_H_INCLUDED__