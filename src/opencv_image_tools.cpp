#include <mbzirc_commons/tools/opencv_image_tools.h>
#include <string>
#include <highgui.h>
#include <cstdio>

namespace mbzirc
{
namespace commons
{

  // Prints a text on an image on a specified (bottom-left) position
  void OpenCVImageTools::PrintTextOnImage(cv::Mat img_image, char text[], cv::Point text_bottomleft, cv::Scalar color,
					double font_scale, int thickness, int font_face)
  {
    // Calculate the text size and the baseline
    //int baseline = 0;
    //cv::Size textSize = cv::getTextSize(text, font_face, font_scale, thickness, &baseline);

    // Center the text
    //cv::Point textOrg((text_center.x - textSize.width) / 2, (text_center.y + textSize.height) / 2);

    // Draw the text
    cv::putText(img_image, text, text_bottomleft, font_face, font_scale, color, thickness, 8);
  }
  
  // Returns the histogram of a given image (converted to grayscale)
  cv::MatND OpenCVImageTools::CalculateHistogram(cv::Mat &img_image, cv::Mat mask, 
					bool cumulative, int range_start, int range_end)
  {
    // Initialize parameters
    int histSize = range_end - range_start + 1;
    float range[] = {range_start, range_end};
    const float *ranges[] = {range};

    // Convert to grayscale
    cv::Mat img_gray;
    cv::cvtColor(img_image, img_gray, CV_BGR2GRAY);

    // Calculate histogram
    cv::MatND hist;
    cv::calcHist(&img_gray, 1, 0, mask, hist, 1, &histSize, ranges, true, false);
  
    // Calculate cumilative histogram
    if (cumulative == true)
      for (int i = 1; i < histSize; ++i)
	hist.at<float>(i) += hist.at<float>(i - 1);
    
    return hist;
  }
  
  // Returns the histogram image of a given image (converted to grayscale)
  cv::Mat OpenCVImageTools::HistogramImage(cv::Mat &img_image, int bin_width, 
					int range_start, int range_end, cv::Mat mask)
  {
    // Calculate histogram
    cv::MatND hist = CalculateHistogram(img_image, mask, false, range_start, range_end);
    
    // Normalize the histogram
    int histSize = range_end - range_start + 1;
    int hist_width = bin_width * histSize;
    int hist_height = 400;
    int textarea_height = 50;
    cv::normalize(hist, hist, 0, hist_height, cv::NORM_MINMAX, -1, cv::Mat());
    
    // Plot the histogram
    cv::Mat histImage(hist_height + textarea_height, hist_width, CV_8UC1, cv::Scalar(0));
    for (int i = 1; i < histSize; i++)
    {
      cv::line(histImage, cv::Point(bin_width * (i-1), hist_height - cvRound(hist.at<float>(i-1)) ) ,
		      cv::Point(bin_width * i, hist_height - cvRound(hist.at<float>(i))),
		      cv::Scalar(255), 2, 8, 0);
    }
    
    // Print axis labels
    int tick_size = 20;
    for (int i = tick_size; i < histSize; i += tick_size)
    {
      char tickText[5];
      sprintf(tickText, "%i", i);
      PrintTextOnImage(histImage, tickText, cv::Point(bin_width * i, hist_height + textarea_height / 2));
    }
    
    return histImage;
  }
  
  // Crop the image by the rectangle roi
  // Can create a reference to the roi instead of creating new image
  void OpenCVImageTools::CropImage(cv::Mat& src, cv::Mat& dst, cv::Rect roi, bool createReference)
  {
    if (!createReference)
      src(roi).copyTo(dst);
    else
      dst = src(roi);
  }
  
  /// Rotates image "src" for "angle" radians around point "pt" and put result in the "dst"
  void OpenCVImageTools::RotateImage(cv::Mat& src, cv::Mat& dst, double angle, cv::Point2f pt)
  {
      cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);
      cv::warpAffine(src, dst, r, cv::Size(src.cols, src.rows));
  }

  // Saves an image into a file with a pre-formatted name and number
  void OpenCVImageTools::SaveImageToNumberedFile(cv::Mat img_image, std::string path_and_name, 
				      int file_number, std::string replace_string)
  {
    // Replace $num$ with the file number if it is there
    std::size_t pos = path_and_name.find(replace_string);
    if (pos != std::string::npos)
    {
      char num_string[20];
      sprintf(num_string, "%i", file_number);
      path_and_name.replace(pos, std::string(replace_string).length(), num_string);
    }
    cv::imwrite(path_and_name, img_image);
  }


} // end of commons namespace

} // end of mbzirc namespace
