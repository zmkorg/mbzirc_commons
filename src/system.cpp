#include <mbzirc_commons/classes/system.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <cstdlib>
#include <dirent.h>
#include <algorithm>
#include <cstring>

namespace mbzirc
{
namespace commons
{

  std::string System::GetHomeDir()
  {
    const char *homedir = getenv("HOME");
    
    if (homedir == NULL)
	return getpwuid(getuid())->pw_dir;
    
    return homedir;
  }
  
    std::vector<std::string> System::ListSubDirectories(std::string path)
    {
        std::vector<std::string> DirList;
        if (path[path.length() - 1] != '/') path += "/";
        DIR *pdir = opendir(path.c_str());
        struct dirent *entry = readdir(pdir);

        while (entry != NULL)
        {
            if  ((entry->d_type == DT_DIR) && (strncmp(entry->d_name, ".", 1) != 0))
                DirList.push_back(path + entry->d_name + "/");
            entry = readdir(pdir);
        }
        std::sort(DirList.begin(), DirList.end());
        return DirList;  
    }

    bool System::CheckFileExtension(std::string filename, std::string extension)
    {
        if (extension == "") return true;
        if (extension.length() > filename.length()) return false;
        if (filename.substr(filename.length() - extension.length(), std::string::npos) == extension)
            return true;
        return false;
    }

    std::vector<std::string> System::ListFiles(std::string path, std::string extension, std::string exclude_prefix)
    {
        if (path[path.length() - 1] != '/') path += "/";
        std::vector<std::string> FileList;
        DIR *pdir = opendir(path.c_str());
        struct dirent *entry = readdir(pdir);

        while (entry != NULL)
        {
            if  ((entry->d_type != DT_DIR) && (strncmp(entry->d_name, ".", 1) != 0) && (CheckFileExtension(entry->d_name, extension)))
                if ((exclude_prefix != "") && (strncmp(entry->d_name, exclude_prefix.c_str(), exclude_prefix.length()) != 0))
                    FileList.push_back(entry->d_name);
            entry = readdir(pdir);
        }
        std::sort(FileList.begin(), FileList.end());
        return FileList;  
    }



} // end of commons namespace

} // end of mbzirc namespace
