#include <mbzirc_commons/tools/geometry.h>

namespace mbzirc
{
namespace commons
{

  /// Returns the intersect point of two lines
  cv::Point2f Geometry::LinesIntersection(cv::Point2f a1, cv::Point2f b1, cv::Point2f a2, cv::Point2f b2)
  {
    std::complex <double> p1(a1.x, a1.y), p2(b1.x, b1.y), q1(a2.x, a2.y), q2(b2.x, b2.y);
    double x;
    
    //Transition
    p2 -= p1;
    q1 -= p1; q2 -= p1;
    
    //Scale & Rotation
    q1 /= p2; q2 /= p2;
    
    //Intersect
    x = q1.real() + (q1.imag() * abs(q2.real() - q1.real()) / abs(q1.imag() - q2.imag()));
    std::complex <double> c(x, 0);
    c = c * p2 + p1;
    return cv::Point2f(c.real(), c.imag());
  }

  /// Returns the Cartesian coefficients for a line described in polar coordinates
  /// The line will be a*x + by = a*x0 + by0, where (x0, y0) is the returned point
  cv::Point2f Geometry::LinePolarToCartesian(double rho, double theta, double &a, double &b)
  {
    a = cos(theta);
    b = sin(theta);
    return cv::Point2f(a * rho, b * rho);
  }

  /// Returns the distance of a line from a point in cartesian coordinates
  /// The line is described with a*x + b*y = a*x0 + b*y0, where p0 = (x0, y0)
  double Geometry::LineDistFromPoint(double a, double b, cv::Point2f p0, cv::Point2f point)
  {
    return (a * (point.y - p0.y) + b * (point.x - p0.x)) / sqrt(a * a + b * b);
  }
    
} // end of commons namespace

} // end of mbzirc namespace
