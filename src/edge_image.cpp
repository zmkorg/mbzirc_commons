#include <mbzirc_commons/classes/edge_image.h>

namespace mbzirc
{
namespace commons
{

  /// Calculates edges of the image and its contours
  void EdgeImage::CalculateEdges(cv::Mat src, double canny_thresh1, double canny_thresh2, 
	double canny_apperture_size, int min_contour_size, bool exclude_small_contours, int bold_image_thickness)
  {
    // Convert to grayscale
    cv::cvtColor(src, Image, CV_BGR2GRAY);

    /// Reduce noise with a kernel 3x3
    cv::blur(Image, Image, cv::Size(3, 3));
    
    // Perform Canny edge detector
    cv::Canny(Image, Image, canny_thresh1, canny_thresh2, canny_apperture_size);

    // Improve the edges
    cv::findContours(Image, Contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE, cv::Point(0, 0));
    
    // Initialize all contour map to -1
    ContourMap = cv::Mat(Image.rows, Image.cols, CV_32SC1);
    ContourMap.setTo(cv::Scalar(-1));
    
    // Create the edge image and contour map
    Image = cv::Mat::zeros(Image.size(), CV_8UC1);
    BoldImage = cv::Mat::zeros(Image.size(), CV_8UC1);
    for (int i = 0; i < Contours.size(); i++)
    {
      if (Contours[i].size() < min_contour_size) 
      {
	if (exclude_small_contours == false)
	  GoodContours.push_back(i);
	continue;
      }
      GoodContours.push_back(i);
      cv::drawContours(Image, Contours, i, cv::Scalar(255), 1, 8, cv::noArray(), 0, cv::Point());
      cv::drawContours(BoldImage, Contours, i, cv::Scalar(255), bold_image_thickness, 8, cv::noArray(), 0, cv::Point());
      for (int j = 1; j < Contours[i].size() ; ++j) 
	ContourMap.at<int>(Contours[i][j]) = i;
    }
  }

} // end of commons namespace

} // end of mbzirc namespace
