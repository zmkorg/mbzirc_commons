#include <mbzirc_commons/classes/ellipse.h>
#include <mbzirc_commons/tools/opencv_image_tools.h>
#include <mbzirc_commons/tools/logger.h>
#include <ros/ros.h>

namespace mbzirc
{
namespace commons
{

  void Ellipse::Initialize()
  {
    center = cv::Point2f();
    size = cv::Size2f();
    angle = 0.0F;
  }

  Ellipse::Ellipse()
  {
    Initialize();
  }
  
  Ellipse::Ellipse(cv::RotatedRect rect)
  {
    Initialize();
    center = rect.center;
    size = rect.size;
    angle = rect.angle;
  }
  
  bool Ellipse::CreateFromContour(std::vector<cv::Point> &contour, int min_points_num, 
			 double max_axis_ratio, int min_axis_size, int max_axis_size)
  {
    // Initialize parameters
    Initialize();
    
    // Reject the contour if the number of pixels is small
    if (contour.size() < min_points_num) return false;

    // Fit an ellipse to the points in the contour
    *this = cv::fitEllipse(cv::Mat(contour));
    
    // Reject the ellipse if the axes ratio is too high/low
    if (max_axis_ratio > 0)
    {
      double axis_ratio = (double)size.height / (double)size.width;
      if (axis_ratio < (1 / max_axis_ratio) || axis_ratio > max_axis_ratio) return false;
    }
    
    // Reject the ellipse if the axes are too small or too large
    if (size.height > max_axis_size || size.width < min_axis_size) return false;
    
    return true;
  }
  
  void Ellipse::DrawEllipse(cv::Mat &ellipse_image, cv::Scalar color, bool isFilled)
  {
    int thickness = (isFilled) ? -1 : 2;
    // Draw ellipse on the image
    cv::ellipse(ellipse_image, *this, color, thickness, 8 );
  }
  
  double Ellipse::OverlapContourOnEllipse(std::vector<cv::Point> &contour, cv::Mat &ellipse_image)
  {
    // Calculate the number of the pixels in the contour overlapped with the ellipse
    int masked_pixels = 0;
    for (int i = 0; i < contour.size(); ++i)
      if (ellipse_image.at<char>(contour[i]) != 0)
	masked_pixels++;

    // Calculate the score for the ellipse
    return (double)masked_pixels / contour.size();
  }
  
  // NOTE: Also changes the ellipse_image input
  double Ellipse::OverlapEllipseOnEdgeImage(cv::Mat &ellipse_image, cv::Mat &edge_image)
  {
    // Calculate the number of the pixels in the contour overlapped with the ellipse
    int masked_pixels = 0;

    // Calculate the number of pixels on the ellipse overlapped with the edge image
    int ellipse_pixels = cv::countNonZero(ellipse_image);
    cv::bitwise_and(ellipse_image, edge_image, ellipse_image);
    masked_pixels = cv::countNonZero(ellipse_image);

    // Calculate the score for the ellipse
    return (double)masked_pixels / ellipse_pixels;
  }
  
  // Extracts the part of input image specified by the ellipse. Optionally can crop it to the ROI
  bool Ellipse::ExtractEllipseFromImage(cv::Mat &original_image, cv::Mat &ellipse_region, bool crop, bool remove_borders, int border_size)
  {
    LOG_INFO("Called ExtractEllipseFromImage(image, %s, %s, %i)", crop ? "true" : "false", remove_borders ? "true" : "false", border_size);
    
    LOG_INFO("Input ellipse:\tWidth: %0.1f\tHeight:%0.1f", size.width, size.height);
    
    if (size.width <= 0)
    {
      LOG_INFO("Returning empty image...");
      ellipse_region = cv::Mat::zeros(original_image.size(), CV_8UC1);
      return false;
    }
    
    Ellipse new_ellipse = Ellipse(*this);
    if (remove_borders == true)
    {
      new_ellipse.size.height -= border_size;
      new_ellipse.size.width -= border_size;
    }
    
    LOG_INFO("New ellipse:\tWidth: %0.1f\tHeight:%0.1f", new_ellipse.size.width, new_ellipse.size.height);

    // Crop if needed
    if (crop == true)
    {
      cv::Point crop_offset;
      return new_ellipse.ExtractEllipseFromImage(original_image, ellipse_region, crop_offset);
    }

    // Create the elliptic mask
    cv::Mat ellipse_mask = cv::Mat::zeros(original_image.size(), CV_8UC1);
    new_ellipse.DrawEllipse(ellipse_mask, cv::Scalar(255), true);

    // Extract the elliptic part
    LOG_INFO("Extract the elliptic part...")
    original_image.copyTo(ellipse_region, ellipse_mask);
    LOG_INFO("Elliptic part extracted...")
        
    // Return the result
    return true;
  }
  
  // Extracts the part of input image specified by the ellipse. Crops it to the ROI and returns the offset
  bool Ellipse::ExtractEllipseFromImage(cv::Mat &original_image, cv::Mat &ellipse_region, cv::Point &crop_offset)
  {
    LOG_INFO("Called ExtractEllipseFromImage(image, (%i, %i))", crop_offset.x, crop_offset.y);

    
    if (ExtractEllipseFromImage(original_image, ellipse_region, false, false) == false)
        return false;

    // Calculate the crop area
    cv::Rect bounding_rect = BoundingRectAdjusted(original_image.cols, original_image.rows);
    
    // Crop the image
    crop_offset.x = bounding_rect.x;
    crop_offset.y = bounding_rect.y;
    LOG_INFO("Cropping image to ellipse boundaries:");
    LOG_INFO("Ellipse bounding rect: x = %i, y = %i, width = %i, height = %i", bounding_rect.x, bounding_rect.y, bounding_rect.width, bounding_rect.height);
    OpenCVImageTools::CropImage(ellipse_region, ellipse_region, bounding_rect);
    LOG_INFO("Image cropped to ellipse boundaries...");
    return true;
  }
  
  // Returns the bounding upright rectangle of the ellipse within the given image size
  cv::Rect Ellipse::BoundingRectAdjusted(int image_width, int image_height)
  {
    LOG_INFO("Called BoundingRectAdjusted(%i, %i)", image_width, image_height);

    cv::Rect bounding_rect = this->boundingRect();
    LOG_INFO("RotatedRect bounding rect: x = %i, y = %i, width = %i, height = %i", bounding_rect.x, bounding_rect.y, bounding_rect.width, bounding_rect.height);
    if (bounding_rect.x >= 0)
        bounding_rect.width = cv::min(bounding_rect.width, image_width - bounding_rect.x);
    else
    {
        bounding_rect.width = cv::min(bounding_rect.width + bounding_rect.x, image_width);
        bounding_rect.x = 0;
    }
    if (bounding_rect.y >= 0)
        bounding_rect.height = cv::min(bounding_rect.height, image_height - bounding_rect.y);
    else
    {
        bounding_rect.height = cv::min(bounding_rect.height + bounding_rect.y, image_height);
        bounding_rect.y = 0;
    }
    return bounding_rect;
  }
  
} // end of commons namespace

} // end of mbzirc namespace
